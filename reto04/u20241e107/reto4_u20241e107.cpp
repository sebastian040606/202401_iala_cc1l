//funcio que cree una matriz n*m

#include<iostream>

using namespace std;

int** matrixDinamica(int n, int m) {

	int** matrix = new int* [n];
	for (int i = 0; i < n; i++) {

		matrix[i] = new int[m];
		for (int j = 0; j < m; j++) {

			matrix[i][j] = 0;
		}
	}
	return matrix;
}
void imprimirMatrix(int ** matrix, int n, int m) {

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << *(*(matrix + i) + j) << ", ";

		}	
		cout << "\n";
	}
}

int main() {
	int n; //filas
	int m; // columnas
	cout << " ingrese el numeor de filas: "; cin >> n;
	cout << " ingrese las columnas de su matriz: "; cin >> m;
	int** matrix = matrixDinamica(n, m);
	imprimirMatrix(matrix, n, m);

	
	for (int i = 0; i < n; i++) {
		delete[] matrix[i];
	}
	delete[] matrix;

	return 0;
}