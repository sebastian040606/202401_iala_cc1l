#include <iostream>

using namespace std;

int** create_array(int n, int m) {
    int** array = new int* [n];

    for (int i = 0; i < n; ++i) {
        array[i] = new int[m];
        for (int j = 0; j < m; ++j) {
            array[i][j] = 0;
        }
    }

    return array;
}

void poner_matriz(int** array, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << array[i][j] << " ";
        }
        cout << endl;
    }
}

void delete_matriz(int** array, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] array[i];
    }
    delete[] array;
}

int main() {
    int n;
    int m;

    cout << "Ingrese n: ";
    cin >> n;
    cout << "Ingrese m: ";
    cin >> m;
    int** array = create_array(n, m);

    cout << "Matriz inicializada con ceros:" << endl;
    poner_matriz(array, n, m);

    delete_matriz(array, n);
    system("pause");
    return 0;
}